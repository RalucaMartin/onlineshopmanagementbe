﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopDataAccess.Interfaces
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();
        T Add(T entity);
        T Find(Guid id);
        T Delete(T entity);
        void DeleteAll(Guid UserId);
    }
}
