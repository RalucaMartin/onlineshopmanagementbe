﻿using OnlineShopDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopDataAccess.Repositories
{
    class NewsRepository : IRepository<News>
    {
        private OnlineShopEntities _db;

        public NewsRepository(OnlineShopEntities db)
        {
            _db = db;
        }

        public News Add(News entity)
        {
            var news = _db.News.Add(entity);
            return news;
        }

        public News Delete(News entity)
        {
            var news = _db.News.Remove(entity);
            return news;
        }

        public void DeleteAll(Guid NewsId)
        {
            throw new NotImplementedException();
        }

        public News Find(Guid id)
        {
            var news = _db.News.Find(id);
            return news;
        }

        public IQueryable<News> GetAll()
        {
            var list = _db.News;
            return list;
        }
}
}
