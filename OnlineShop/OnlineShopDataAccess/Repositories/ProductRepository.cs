﻿using OnlineShopDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopDataAccess.Repositories
{
    class ProductRepository : IRepository<Product>
    {
        private OnlineShopEntities _db;

        public ProductRepository(OnlineShopEntities db)
        {
            _db = db;
        }

        public Product Add(Product entity)
        {
            var product = _db.Products.Add(entity);
            return product;
        }

        public Product Delete(Product entity)
        {
            var product = _db.Products.Remove(entity);
            return product;
        }

        public void DeleteAll(Guid UserId)
        {
            throw new NotImplementedException();
        }

        public Product Find(Guid id)
        {
            var product = _db.Products.Find(id);
            return product;
        }

        public IQueryable<Product> GetAll()
        {
            var list = _db.Products;
            return list;
        }
    }
}
