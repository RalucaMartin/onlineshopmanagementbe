﻿using OnlineShopDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopDataAccess.Repositories
{
    class OrderItemRepository : IRepository<OrderItem>
    {
        private OnlineShopEntities _db;

        public OrderItemRepository(OnlineShopEntities db)
        {
            _db = db;
        }

        public OrderItem Add(OrderItem entity)
        {
            var orderItem = _db.OrderItems.Add(entity);
            return orderItem;
        }

        public OrderItem Delete(OrderItem entity)
        {
            var orderItem = _db.OrderItems.Remove(entity);
            return orderItem;
        }

        public void DeleteAll(Guid OrderItemId)
        {
            throw new NotImplementedException();
        }

        public OrderItem Find(Guid id)
        {
            var orderItem = _db.OrderItems.Find(id);
            return orderItem;
        }

        public IQueryable<OrderItem> GetAll()
        {
            var list = _db.OrderItems;
            return list;
        }
    }
}
