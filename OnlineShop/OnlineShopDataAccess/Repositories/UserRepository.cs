﻿using OnlineShopDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopDataAccess.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private OnlineShopEntities _db;

        public UserRepository(OnlineShopEntities db)
        {
            _db = db;
        }

        public User Add(User entity)
        {
            var user = _db.Users.Add(entity);
            return user;
        }

        public User Delete(User entity)
        {
            var user = _db.Users.Remove(entity);
            return user;
        }

        public void DeleteAll(Guid UserId)
        {
            throw new NotImplementedException();
        }

        public User Find(Guid id)
        {
            var user = _db.Users.Find(id);
            return user;
        }

        public IQueryable<User> GetAll()
        {
            var list = _db.Users;
            return list;
        }
    }
}
