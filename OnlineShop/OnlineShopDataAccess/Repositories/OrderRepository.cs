﻿using OnlineShopDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopDataAccess.Repositories
{
    class OrderRepository : IRepository<Order>
    {
        private OnlineShopEntities _db;

        public OrderRepository(OnlineShopEntities db)
        {
            _db = db;
        }

        public Order Add(Order entity)
        {
            var order = _db.Orders.Add(entity);
            return order;
        }

        public Order Delete(Order entity)
        {
            var order = _db.Orders.Remove(entity);
            return order;
        }

        public void DeleteAll(Guid UserId)
        {
            throw new NotImplementedException();
        }

        public Order Find(Guid id)
        {
            var order = _db.Orders.Find(id);
            return order;
        }

        public IQueryable<Order> GetAll()
        {
            var order = _db.Orders;
            return order;
        }
    }
}
