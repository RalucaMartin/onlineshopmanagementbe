﻿using OnlineShopDataAccess.Interfaces;
using OnlineShopDataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopDataAccess.UnitOfWork
{
    public class UnitOfWork
    {
        private OnlineShopEntities _context = null;

        public UnitOfWork(OnlineShopEntities context)
        {
            _context = context;
        }

        IRepository<Product> productRepository = null;
        IRepository<User> userRepository = null;
        IRepository<OrderItem> orderItemRepository = null;
        IRepository<Order> orderRepository = null;
        IRepository<News> newsRepository = null;
        public IRepository<Product> ProductRepository
        {
            get
            {
                if (productRepository == null)
                {
                    productRepository = new ProductRepository(_context);
                }
                return productRepository;
            }
        }

        public IRepository<User> UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(_context);
                }
                return userRepository;
            }
        }

        public IRepository<OrderItem> OrderItemRepository
        {
            get
            {
                if (orderItemRepository == null)
                {
                    orderItemRepository = new OrderItemRepository(_context);
                }
                return orderItemRepository;
            }
        }

        public IRepository<Order> OrderRepository
        {
            get
            {
                if (orderRepository == null)
                {
                    orderRepository = new OrderRepository(_context);
                }
                return orderRepository;
            }
        }

        public IRepository<News> NewsRepository
        {
            get
            {
                if (newsRepository == null)
                {
                    newsRepository = new NewsRepository(_context);
                }
                return newsRepository;
            }
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
