﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.Models
{
    class OrderItemModel
    {
        public System.Guid Id { get; set; }
        public System.Guid OrderId { get; set; }
        public string ProductName { get; set; }
        public Guid ProductId { get; set; }
        public int NrOfItems { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
