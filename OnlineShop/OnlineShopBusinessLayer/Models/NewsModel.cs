﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.Models
{
    public class NewsModel
    {
        public System.Guid Id { get; set; }
        public string Comment { get; set; }
    }
}
