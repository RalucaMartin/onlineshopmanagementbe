﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.Models
{
    public class ProductModel
    {
     
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int? Quantity { get; set; }
            public decimal? Price { get; set; }
            public string Description { get; set; }
        
    }

    public class ProductGuidModel
    {
        public Guid Id { get; set; }
        public Guid userId { get; set; }
    }

    public class ProductUpdateModel
    {
        public Guid Id { get; set; }
        public decimal Price { get; set; }
    }
}
