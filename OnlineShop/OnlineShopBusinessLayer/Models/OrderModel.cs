﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.Models
{
    class OrderModel
    {
        public System.Guid Id { get; set; }
        public System.Guid UserId { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
