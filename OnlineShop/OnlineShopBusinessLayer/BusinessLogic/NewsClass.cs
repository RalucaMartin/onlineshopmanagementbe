﻿using Newtonsoft.Json.Linq;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using OnlineShopDataAccess.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.BusinessLogic
{
    public class NewsClass
    {
        private UnitOfWork _unitOfWork;

        public NewsClass(OnlineShopEntities _context)
        {
            _unitOfWork = new UnitOfWork(_context);
        }

        public List<NewsModel> GetNews()
        {
            List<NewsModel> newsList = new List<NewsModel>();

            var news = _unitOfWork.NewsRepository.GetAll();

            foreach (var newsrubric in news)
            {
                NewsModel newsModel = new NewsModel();
                newsModel.Id = newsrubric.id;
                newsModel.Comment = newsrubric.Comment;
                newsList.Add(newsModel);
            }
            return newsList;
        }

        public bool NewsExists(string comment)
        {
            var result = _unitOfWork.NewsRepository.GetAll().Any(n => n.Comment == comment);
            return result;
        }

        public JObject AddNews(NewsModel newsModel)
        {
            JObject tokenResponse = new JObject();

            var isNewsDb = NewsExists(newsModel.Comment);

            if (isNewsDb)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "This news already exist"));
                tokenResponse.Add(new JProperty("exception", string.Empty));

                return tokenResponse;
            }

            News newNews = new News();
            newNews.id = Guid.NewGuid();
            newNews.Comment = newsModel.Comment;

            try
            {
                _unitOfWork.NewsRepository.Add(newNews);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There was a problem in saving the news in database"));
                tokenResponse.Add(new JProperty("exception", ex.ToString()));

                return tokenResponse;
            }

            tokenResponse.Add(new JProperty("success", "true"));
            tokenResponse.Add(new JProperty("message", "The news has been successfully added."));
            tokenResponse.Add(new JProperty("exception", ""));

            return tokenResponse;
        }
    }
}
