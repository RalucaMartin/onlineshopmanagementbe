﻿using Newtonsoft.Json.Linq;
using OnlineShopDataAccess;
using OnlineShopDataAccess.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.BusinessLogic
{
    public class OrderItemClass
    {
        private UnitOfWork _unitOfWork;

        public OrderItemClass(OnlineShopEntities _context)
        {
            _unitOfWork = new UnitOfWork(_context);
        }

        public JObject AddProductToOrder(Guid prodId, Guid userId)
        {
            JObject tokenResponse = new JObject();

            var orderId = _unitOfWork.OrderRepository.GetAll().Where(o => o.UserId == userId).SingleOrDefault();

            if (orderId == null)
            {
                Order orderModel = new Order();
                orderModel.Id = Guid.NewGuid();
                orderModel.UserId = userId;
                orderModel.TotalAmount = 0;
                _unitOfWork.OrderRepository.Add(orderModel);
                _unitOfWork.Save();
            }

            var orderItems = _unitOfWork.OrderItemRepository.GetAll().Where(o => o.OrderId == orderId.Id && o.ProductId == prodId).SingleOrDefault();

            if (orderItems != null)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "This item already exist in the shopping chart"));
                tokenResponse.Add(new JProperty("exception", string.Empty));

                return tokenResponse;
            }

            var product = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == prodId).SingleOrDefault();

            if (product.Quantity == 0)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "This item doesn't exist anymore"));
                tokenResponse.Add(new JProperty("exception", string.Empty));

                return tokenResponse;
            }

            var order = _unitOfWork.OrderRepository.GetAll().Where(o => o.Id == orderId.Id).SingleOrDefault();

            OrderItem ordModel = new OrderItem();
            ordModel.Id = Guid.NewGuid();
            ordModel.OrderId = orderId.Id;
            ordModel.ProductId = prodId;
            ordModel.NrOfItems = 1;
            ordModel.TotalAmount = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == prodId).SingleOrDefault().Price;

            product.Quantity = product.Quantity - 1;

            try
            {
                _unitOfWork.OrderItemRepository.Add(ordModel);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There was a problem in saving the product in database"));
                tokenResponse.Add(new JProperty("exception", ex.ToString()));

                return tokenResponse;
            }

            tokenResponse.Add(new JProperty("success", "true"));
            tokenResponse.Add(new JProperty("message", "The product has been successfully added."));
            tokenResponse.Add(new JProperty("exception", ""));

            return tokenResponse;
        }

        public JObject DeleteProductFormOrder(Guid productId, Guid userId)
        {
            var order = _unitOfWork.OrderRepository.GetAll().Where(o => o.UserId == userId).SingleOrDefault();
            var orderItem = _unitOfWork.OrderItemRepository.GetAll().Where(p => p.ProductId == productId && p.OrderId == order.Id).SingleOrDefault();

            JObject tokenResponse = new JObject();

            var product = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == productId).SingleOrDefault();
            product.Quantity = product.Quantity + orderItem.NrOfItems;

            order.TotalAmount = order.TotalAmount - orderItem.TotalAmount;

            if (order.TotalAmount < 0)
            {
                order.TotalAmount = 0;
            }

            try
            {
                _unitOfWork.OrderItemRepository.Delete(orderItem);

                _unitOfWork.Save();

                tokenResponse.Add(new JProperty("success", "true"));
                tokenResponse.Add(new JProperty("message", "The product was deleted successfully"));
                tokenResponse.Add(new JProperty("exception", ""));

                return tokenResponse;
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There was a problem in deleting this product form database"));
                tokenResponse.Add(new JProperty("exception", ex.Message));

                return tokenResponse;
            }
        }

    }
}
