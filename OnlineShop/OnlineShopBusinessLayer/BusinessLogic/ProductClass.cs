﻿using Newtonsoft.Json.Linq;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using OnlineShopDataAccess.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.BusinessLogic
{
    public class ProductClass
    {
        private UnitOfWork _unitOfWork;

        public ProductClass(OnlineShopEntities _context)
        {
            _unitOfWork = new UnitOfWork(_context);
        }

        public bool ProductExists(string name)
        {
            var result = _unitOfWork.ProductRepository.GetAll().Any(p => p.Name == name);
            return result;
        }

        public JObject AddProduct(ProductModel productModel)
        {
            JObject tokenResponse = new JObject();

            var isProductDb = ProductExists(productModel.Name);

            if (isProductDb)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "This product already exist"));
                tokenResponse.Add(new JProperty("exception", string.Empty));

                return tokenResponse;
            }

            Product newProduct = new Product();

            newProduct.Id = Guid.NewGuid();
            newProduct.Name = productModel.Name;
            newProduct.Quantity = productModel.Quantity;
            newProduct.Price = productModel.Price;
            newProduct.Description = productModel.Description;

            try
            {
                _unitOfWork.ProductRepository.Add(newProduct);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There was a problem in saving the product in database"));
                tokenResponse.Add(new JProperty("exception", ex.ToString()));

                return tokenResponse;
            }

            tokenResponse.Add(new JProperty("success", "true"));
            tokenResponse.Add(new JProperty("message", "The product has been successfully added."));
            tokenResponse.Add(new JProperty("exception", ""));

            return tokenResponse;
        }

        public List<ProductModel> GetProducts()
        {
            List<ProductModel> productList = new List<ProductModel>();

            var products = _unitOfWork.ProductRepository.GetAll();

            foreach (var product in products)
            {
                ProductModel prodModel = new ProductModel();
                prodModel.Id = product.Id;
                prodModel.Name = product.Name;
                prodModel.Quantity = product.Quantity;
                prodModel.Price = product.Price;
                prodModel.Description = product.Description;
                productList.Add(prodModel);
            }
            return productList;
        }

        public ProductModel GetProduct(Guid productId)
        {
            var product = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == productId).FirstOrDefault();

            if (product != null)
            {
                ProductModel prodModel = new ProductModel();
                prodModel.Id = product.Id;
                prodModel.Name = product.Name;
                prodModel.Quantity = product.Quantity;
                prodModel.Price = product.Price;
                prodModel.Description = product.Description;

                return prodModel;
            }
            else
            {
                return null;
            }

        }

        public JObject EditProductDetalis(ProductUpdateModel productModel)
        {
            var product = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == productModel.Id).SingleOrDefault();

            product.Name = product.Name;
            product.Quantity = product.Quantity;
            product.Price = productModel.Price;
            product.Description = product.Description;

            JObject tokenResponse = new JObject();

            try
            {
                _unitOfWork.Save();

                tokenResponse.Add(new JProperty("success", "true"));
                tokenResponse.Add(new JProperty("message", "The product has been successfully edited"));
                tokenResponse.Add(new JProperty("exception", ""));

                return tokenResponse;
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There is an error in saving the product details."));
                tokenResponse.Add(new JProperty("exception", ex.InnerException.ToString()));

                return tokenResponse;
            }
        }

        public JObject EditProductQuantity(ProductModel productModel)
        {
            var product = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == productModel.Id).SingleOrDefault();

            product.Quantity = productModel.Quantity;

            JObject tokenResponse = new JObject();

            try
            {
                _unitOfWork.Save();

                tokenResponse.Add(new JProperty("success", "true"));
                tokenResponse.Add(new JProperty("message", "The product has been successfully edited"));
                tokenResponse.Add(new JProperty("exception", ""));

                return tokenResponse;
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There is an error in saving the product details."));
                tokenResponse.Add(new JProperty("exception", ex.InnerException.ToString()));

                return tokenResponse;
            }
        }

        public JObject DeleteProduct(Guid productId, Guid userId)
        {
            var orderItems = _unitOfWork.OrderItemRepository.GetAll().Where(o => o.ProductId == productId);
            var product = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == productId).SingleOrDefault();

            JObject tokenResponse = new JObject();

            try
            {
                if (product != null)
                {
                    foreach (var ord in orderItems)
                    {
                        var order = _unitOfWork.OrderRepository.GetAll().Where(o => o.Id == ord.OrderId).SingleOrDefault();
                        order.TotalAmount = order.TotalAmount - ord.TotalAmount;
                        _unitOfWork.OrderItemRepository.Delete(ord);
                    }

                    _unitOfWork.ProductRepository.Delete(product);

                    //delete din comanda

                    _unitOfWork.Save();

                    tokenResponse.Add(new JProperty("success", "true"));
                    tokenResponse.Add(new JProperty("message", "The product was deleted successfully"));
                    tokenResponse.Add(new JProperty("exception", ""));

                    return tokenResponse;
                }
                else
                {
                    tokenResponse.Add(new JProperty("success", "false"));
                    tokenResponse.Add(new JProperty("message", "The product doesn't exist"));
                    tokenResponse.Add(new JProperty("exception", ""));

                    return tokenResponse;
                }
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There was a problem in deleting this product form database"));
                tokenResponse.Add(new JProperty("exception", ex.Message));

                return tokenResponse;
            }
        }
    }
}
