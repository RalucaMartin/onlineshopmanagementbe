﻿using Newtonsoft.Json.Linq;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using OnlineShopDataAccess.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.BusinessLogic
{
    public class OrderClass
    {
        private UnitOfWork _unitOfWork;

        public OrderClass(OnlineShopEntities _context)
        {
            _unitOfWork = new UnitOfWork(_context);
        }


        public JObject SendEmail(Guid userId)
        {

            JObject tokenResponse = new JObject();

            var user = _unitOfWork.UserRepository.GetAll().Where(u => u.Id == userId).SingleOrDefault();
            var order = _unitOfWork.OrderRepository.GetAll().Where(u => u.UserId == userId).SingleOrDefault();
            var orderItems = _unitOfWork.OrderItemRepository.GetAll().Where(o => o.OrderId == order.Id);

            List<ProductMailModel> products = new List<ProductMailModel>();

            foreach (var product in orderItems)
            {
                ProductMailModel prod = new ProductMailModel();
                prod.ProductName = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == product.ProductId).SingleOrDefault().Name;
                prod.Quantity = product.NrOfItems;
                prod.Price = product.TotalAmount;
                products.Add(prod);
            }

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("ladela8@gmail.com");
                mail.To.Add(user.Email);
                mail.Subject = "Your order";
                mail.Body = "Your order has been accepted\n" + "Total amount: " + order.TotalAmount.ToString();
                //mail.Body = mail.Body + order.TotalAmount.ToString();

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("ladela8@gmail.com", "mapamapa");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);


                tokenResponse.Add(new JProperty("success", "true"));
                tokenResponse.Add(new JProperty("message", "Email sent"));
                tokenResponse.Add(new JProperty("exception", string.Empty));

                return tokenResponse;
            }
            catch (Exception ex)
            {

                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "Error in sending the email"));
                tokenResponse.Add(new JProperty("exception", ex.ToString()));

                return tokenResponse;
            }
        }
    }
}
