﻿using Newtonsoft.Json.Linq;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using OnlineShopDataAccess.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopBusinessLayer.BusinessLogic
{
    public class UserClass
    {
        private UnitOfWork _unitOfWork;

        public UserClass(OnlineShopEntities _context)
        {
            _unitOfWork = new UnitOfWork(_context);
        }

        public List<UserModel> GetUsers()
        {
            List<UserModel> userList = new List<UserModel>();

            var users = _unitOfWork.UserRepository.GetAll();

            foreach (var user in users)
            {
                UserModel usrModel = new UserModel();
                usrModel.Id = user.Id;
                usrModel.FirstName = user.FirstName;
                usrModel.LastName = user.LastName;
                usrModel.Email = user.Email;
                usrModel.Username = user.Username;
                usrModel.Password = user.Password;
                usrModel.Role = user.Role;
                userList.Add(usrModel);
            }
            return userList;
        }

        public bool UserExists(string username)
        {
            var result = _unitOfWork.UserRepository.GetAll().Any(u => u.Username == username);
            return result;
        }

        public JObject AddUser(UserModel userModel)
        {
            JObject tokenResponse = new JObject();

            var isUserDb = UserExists(userModel.Username);

            if (isUserDb)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "This user already exist"));
                tokenResponse.Add(new JProperty("exception", string.Empty));

                return tokenResponse;
            }

            User newUser = new User();
            newUser.Id = Guid.NewGuid();
            newUser.FirstName = userModel.FirstName;
            newUser.LastName = userModel.LastName;
            newUser.Email = userModel.Email;
            newUser.Username = userModel.Username;
            newUser.Password = userModel.Password;
            newUser.Role = userModel.Role;

            try
            {
                _unitOfWork.UserRepository.Add(newUser);
                _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There was a problem in saving the user in database"));
                tokenResponse.Add(new JProperty("exception", ex.ToString()));

                return tokenResponse;
            }

            tokenResponse.Add(new JProperty("success", "true"));
            tokenResponse.Add(new JProperty("message", "The user has been successfully added."));
            tokenResponse.Add(new JProperty("exception", ""));

            return tokenResponse;
        }

        public bool UserExists(string username, string password)
        {
            var result = _unitOfWork.UserRepository.GetAll().Any(u => u.Username == username && u.Password == password);
            return result;
        }

        public UserModel GetUserByUsernameAndPass(string username, string password)
        {
            var user = _unitOfWork.UserRepository.GetAll().Where(u => u.Username.ToLower().Equals(username.ToLower()) && u.Password.Equals(password)).FirstOrDefault();

            UserModel usrModel = new UserModel();
            usrModel.Id = user.Id;
            usrModel.FirstName = user.FirstName;
            usrModel.LastName = user.LastName;
            usrModel.Email = user.Email;
            usrModel.Username = user.Username;
            usrModel.Password = user.Password;
            usrModel.Role = user.Role;

            return usrModel;

        }

        public JObject EditUserDetalis(UserModel userModel)
        {
            var user = _unitOfWork.UserRepository.GetAll().Where(u => u.Id == userModel.Id).SingleOrDefault();

            user.FirstName = userModel.FirstName;
            user.LastName = userModel.LastName;
            user.Email = userModel.Email;
            user.Username = userModel.Username;
            user.Password = userModel.Password;
            user.Role = userModel.Role;

            JObject tokenResponse = new JObject();

            try
            {
                _unitOfWork.Save();

                tokenResponse.Add(new JProperty("success", "true"));
                tokenResponse.Add(new JProperty("message", "The user has been successfully edited"));
                tokenResponse.Add(new JProperty("exception", ""));

                return tokenResponse;
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There is an error in saving the user details."));
                tokenResponse.Add(new JProperty("exception", ex.InnerException.ToString()));

                return tokenResponse;
            }
        }

        public JObject DeleteUser(Guid userId)
        {
            var order = _unitOfWork.OrderRepository.GetAll().Where(o => o.UserId == userId).SingleOrDefault();
            var orderItems = _unitOfWork.OrderItemRepository.GetAll().Where(o => o.OrderId == order.Id);
            var user = _unitOfWork.UserRepository.GetAll().Where(u => u.Id == userId).SingleOrDefault();

            JObject tokenResponse = new JObject();

            try
            {
                if (user != null)
                {
                    if (order != null)
                    {
                        foreach (var ord in orderItems)
                        {
                            var product = _unitOfWork.ProductRepository.GetAll().Where(p => p.Id == ord.ProductId).SingleOrDefault();
                            product.Quantity = product.Quantity + ord.NrOfItems;
                            _unitOfWork.OrderItemRepository.Delete(ord);
                        }

                        _unitOfWork.OrderRepository.Delete(order);
                    }
                    _unitOfWork.UserRepository.Delete(user);

                    _unitOfWork.Save();

                    tokenResponse.Add(new JProperty("success", "true"));
                    tokenResponse.Add(new JProperty("message", "The user was deleted successfully"));
                    tokenResponse.Add(new JProperty("exception", ""));

                    return tokenResponse;
                }
                else
                {
                    tokenResponse.Add(new JProperty("success", "false"));
                    tokenResponse.Add(new JProperty("message", "The user doesn't exist"));
                    tokenResponse.Add(new JProperty("exception", ""));

                    return tokenResponse;
                }
            }
            catch (Exception ex)
            {
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "There was a problem in deleting this user form database"));
                tokenResponse.Add(new JProperty("exception", ex.Message));

                return tokenResponse;
            }
        }
        public UserModel GetUser(Guid userId)
        {
            var user = _unitOfWork.UserRepository.GetAll().Where(u => u.Id == userId).FirstOrDefault();

            if (user != null)
            {
                UserModel usrModel = new UserModel();
                usrModel.Id = user.Id;
                usrModel.FirstName = user.FirstName;
                usrModel.LastName = user.LastName;
                usrModel.Email = user.Email;
                usrModel.Username = user.Username;
                usrModel.Password = user.Password;
                usrModel.Role = user.Role;

                return usrModel;
            }
            else
            {
                return null;
            }

        }
    }
}
