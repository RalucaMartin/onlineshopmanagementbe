﻿using Newtonsoft.Json.Linq;
using OnlineShopBusinessLayer.BusinessLogic;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace OnlineShop.Controllers
{
    public class OrderItemsController : ApiController
    {
        private OrderItemClass _orderItem;
        private UserClass _user;

        [HttpPost]
        public IHttpActionResult AddNewProductToOrder([FromBody] ProductGuidModel prodId)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _orderItem = new OrderItemClass(new OnlineShopEntities());

            return Ok(_orderItem.AddProductToOrder(prodId.Id, prodId.userId));
        }

        [HttpPost]
        public JObject DeleteProductFromOrder([FromBody] ProductGuidModel prod)
        {
            if (prod.Id != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                _orderItem = new OrderItemClass(new OnlineShopEntities());

                return _orderItem.DeleteProductFormOrder(prod.Id, prod.userId);
            }
            else
            {
                JObject tokenResponse = new JObject();
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "UserId is null"));
                tokenResponse.Add(new JProperty("exception", ""));

                return tokenResponse;
            }
        }

    }
}