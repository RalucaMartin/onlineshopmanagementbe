﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using OnlineShopBusinessLayer.BusinessLogic;
using Newtonsoft.Json.Linq;

namespace OnlineShop.Controllers
{
    public class ProductsController : ApiController 
    {
        private ProductClass _product;
        

        [HttpPost]
        public IHttpActionResult AddNewProduct([FromBody] ProductModel productModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _product = new ProductClass(new OnlineShopEntities());

            return Ok(_product.AddProduct(productModel));
        }

        [HttpGet]
        public List<ProductModel> GetProducts()
        {
            _product = new ProductClass(new OnlineShopEntities());
            OnlineShopEntities _db = new OnlineShopEntities();

            return _product.GetProducts();
        }

        [HttpPost]
        public ProductModel GetProduct([FromBody] ProductGuidModel productId)
        {
            _product = new ProductClass(new OnlineShopEntities());

            return _product.GetProduct(productId.Id);
        }

        [HttpPost]
        public IHttpActionResult EditProductDetails([FromBody] ProductUpdateModel productModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _product = new ProductClass(new OnlineShopEntities());

            return Ok(_product.EditProductDetalis(productModel));
        }

        [HttpPost]
        public IHttpActionResult EditProductQuantity([FromBody] ProductModel productModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _product = new ProductClass(new OnlineShopEntities());

            return Ok(_product.EditProductQuantity(productModel));
        }

        [HttpPost]
        public JObject DeleteProduct([FromBody] ProductGuidModel product)
        {
            if (product.Id != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                _product = new ProductClass(new OnlineShopEntities());
                return _product.DeleteProduct(product.Id, product.userId);
            }
            else
            {
                var tokenResponse = new JObject();
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "ProductId is null"));
                tokenResponse.Add(new JProperty("exception", ""));

                return tokenResponse;
            }
        }
    }
}