﻿using Newtonsoft.Json.Linq;
using OnlineShopBusinessLayer.BusinessLogic;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineShop.Controllers
{
    public class UserController : ApiController
    {
        private UserClass _user;

        [HttpGet]
        public List<UserModel> GetUsers()
        {
            _user = new UserClass(new OnlineShopEntities());
            OnlineShopEntities _db = new OnlineShopEntities();

            return _user.GetUsers();
        }

        [HttpPost]
        public UserModel GetUser([FromBody] UserGuidModel userId)
        {
            _user = new UserClass(new OnlineShopEntities());

            return _user.GetUser(userId.Id);
        }

        [HttpPost]
        public IHttpActionResult AddNewUser([FromBody] UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _user = new UserClass(new OnlineShopEntities());

            return Ok(_user.AddUser(userModel));
        }

        [HttpPost]
        public JObject LogIn([FromBody] UserLoginModel userLogin)
        {
            JObject tokenResponse = new JObject();

            var username = userLogin.Username.ToLower();

            if (!UserExists(username, userLogin.Password))
            {
                tokenResponse = new JObject(new JProperty("login", "failed"),
                   new JProperty("message", "WrongCredentials")
               );

                return tokenResponse;
            }
            else
            {
                UserClass uc = new UserClass(new OnlineShopEntities());
                var userInfo = uc.GetUserByUsernameAndPass(username, userLogin.Password);

                //HttpContext context = HttpContext.Current;
                //context.Session["Id"] = userInfo.Id.ToString();
                //context.Session.Add("Id", userInfo.Id.ToString());
               // uc.saveToSession(userInfo);

                tokenResponse = new JObject(
                   new JProperty("login", "success"),
                   new JProperty("Id", userInfo.Id),
                   new JProperty("userName", userInfo.Username),
                   new JProperty("firstName", userInfo.FirstName),
                   new JProperty("lastName", userInfo.LastName),
                   new JProperty("email", userInfo.Email),
                   new JProperty("role", userInfo.Role)
                   );

                return tokenResponse;
            }
        }

        [HttpGet]
        public bool UserExists(string username, string password)
        {
            _user = new UserClass(new OnlineShopEntities());

            return _user.UserExists(username, password);
        }

        [HttpPost]
        public IHttpActionResult EditUserDetails([FromBody] UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _user = new UserClass(new OnlineShopEntities());

            return Ok(_user.EditUserDetalis(userModel));
        }

        [HttpPost]
        public JObject DeleteUser([FromBody] UserGuidModel user)
        {
            if (user.Id != Guid.Parse("00000000-0000-0000-0000-000000000000"))
            {
                _user = new UserClass(new OnlineShopEntities());
                return _user.DeleteUser(user.Id);
            }
            else
            {
                var tokenResponse = new JObject();
                tokenResponse.Add(new JProperty("success", "false"));
                tokenResponse.Add(new JProperty("message", "UserId is null"));
                tokenResponse.Add(new JProperty("exception", ""));

                return tokenResponse;
            }
        }
    }
}
