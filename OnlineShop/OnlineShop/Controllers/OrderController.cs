﻿using Newtonsoft.Json.Linq;
using OnlineShopBusinessLayer.BusinessLogic;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineShop.Controllers
{
    public class OrderController : ApiController
    {
        private OrderClass _order;

        [HttpPost]
        public JObject SendEmail([FromBody] UserGuidModel user)
        {
            _order = new OrderClass(new OnlineShopEntities());

            return _order.SendEmail(user.Id);
        }
    }
}
