﻿using Newtonsoft.Json.Linq;
using OnlineShopBusinessLayer.BusinessLogic;
using OnlineShopBusinessLayer.Models;
using OnlineShopDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineShop.Controllers
{
    public class NewsController : ApiController
    {
        private NewsClass _news;

        [HttpGet]
        public List<NewsModel> GetNews()
        {
            _news= new NewsClass(new OnlineShopEntities());
            OnlineShopEntities _db = new OnlineShopEntities();

            return _news.GetNews();
        }

        [HttpPost]
        public IHttpActionResult AddNewNews([FromBody] NewsModel newsModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _news= new NewsClass(new OnlineShopEntities());

            return Ok(_news.AddNews(newsModel));
        }
    }
}